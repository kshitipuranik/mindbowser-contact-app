package com.kshiti.mindbowsercontactapp.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.kshiti.mindbowsercontactapp.R;
import com.kshiti.mindbowsercontactapp.adapters.ContactRVAdapter;
import com.kshiti.mindbowsercontactapp.model.ContactsModal;
import com.kshiti.mindbowsercontactapp.model.ContactsModalWithPK;

import java.util.ArrayList;
import java.util.List;

public class ContactsActivity extends AppCompatActivity implements ContactRVAdapter.OnContactClickListener  {
    private ArrayList<ContactsModalWithPK> contactsModalArrayList;
    private RecyclerView contactRV;
    private ContactRVAdapter contactRVAdapter;
    private ProgressBar loadingPB;
    DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
    String photoUriDb = "";
    Button callBtn, smsBtn;
    private static final int REQUEST_PHONE_CALL= 1;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String isContactListExist;
    boolean IsFavourite = false;
    ImageView cancelImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        contactsModalArrayList = new ArrayList<>();
        contactRV = findViewById(R.id.idRVContacts);
        loadingPB = findViewById(R.id.idPBLoading);

        getSupportActionBar().setTitle("All Contacts");
        sharedPreferences= getSharedPreferences("ContactInfoPref", Context.MODE_PRIVATE);
        isContactListExist = sharedPreferences.getString("isContactListExist", "");

        if (!isContactListExist.equals("true")){
            requestPermissions();
        }

        getDataFromDatabase();
        prepareContactRV();
    }

    private void requestPermissions() {
        Dexter.withContext(this)
                .withPermissions(Manifest.permission.READ_CONTACTS,
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.SEND_SMS, Manifest.permission.WRITE_CONTACTS)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                        getContacts();
                        Toast.makeText(ContactsActivity.this, "All the permissions are granted..", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                        permissionToken.continuePermissionRequest();
                    }
                }).withErrorListener(new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
                Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
            }
        })
                .onSameThread().check();
    }
    private void getContacts() {
        String contactId = "";
        String displayName = "";
        String photoUri= "";

        Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                    contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                    Cursor phoneCursor = getContentResolver().query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{contactId},
                            null);
                    if (phoneCursor.moveToNext()) {
                        String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        photoUri = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                        ContactsModal contactsModal = new ContactsModal(displayName, phoneNumber, ""+photoUri, IsFavourite);

                        String pushkey = myRef.push().getKey();
                        myRef.child("Contacts").child(pushkey).setValue(contactsModal);

                    }
                    phoneCursor.close();
                }
            }
        }
        cursor.close();
        loadingPB.setVisibility(View.GONE);
        editor = sharedPreferences.edit();
        editor.putString("isContactListExist", "true");
        editor.apply();
    }

    private void prepareContactRV() {
        contactRVAdapter = new ContactRVAdapter(this, contactsModalArrayList, this::onContactClick);
        contactRV.setLayoutManager(new LinearLayoutManager(this));
        contactRV.setAdapter(contactRVAdapter);
    }

    public void getDataFromDatabase(){

        myRef.child("Contacts").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                contactsModalArrayList.clear();

                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    String userName = (String) dataSnapshot.child("userName").getValue();
                    String contactNo = (String) dataSnapshot.child("contactNumber").getValue();
                    String pushkey = dataSnapshot.getKey();
                    boolean IsFav = (boolean) dataSnapshot.child("favourite").getValue();
                    if (dataSnapshot.hasChild("photoUri")) {
                        photoUriDb = (String) dataSnapshot.child("photoUri").getValue();
                    } else {
                        photoUriDb = "";
                    }

                    ContactsModalWithPK contactsModalWithPK = new ContactsModalWithPK(pushkey, new ContactsModal(userName, contactNo, photoUriDb, IsFav));
                    contactsModalArrayList.add(contactsModalWithPK);
                }

                contactRVAdapter.notifyDataSetChanged();
                loadingPB.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    @Override
    public void onContactClick(int position) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.call_dialog);
        dialog.show();
        callBtn = dialog.findViewById(R.id.callBtn);
        smsBtn = dialog.findViewById(R.id.smsBtn);
        cancelImage = dialog.findViewById(R.id.cancelImg);

        callBtn.setOnClickListener(v -> {
            call(position);
        });

        smsBtn.setOnClickListener(v -> {
            sendSMS(position);
        });

        cancelImage.setOnClickListener(view -> dialog.dismiss());
    }

    private void call(int position)
    {
        String phoneNumber = contactsModalArrayList.get(position).getContactsModal().getContactNumber().trim();
        Log.wtf("phoneNumber",phoneNumber);

        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
        if (ActivityCompat.checkSelfPermission(ContactsActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(ContactsActivity.this, new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
            return;
        }
        startActivity(callIntent);
    }

    private void sendSMS(int position){
        String phoneNumber = contactsModalArrayList.get(position).getContactsModal().getContactNumber().trim();
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phoneNumber, null)));
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}