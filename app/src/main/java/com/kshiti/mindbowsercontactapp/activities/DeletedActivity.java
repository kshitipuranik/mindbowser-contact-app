package com.kshiti.mindbowsercontactapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kshiti.mindbowsercontactapp.R;
import com.kshiti.mindbowsercontactapp.adapters.DeletedRVAdapter;
import com.kshiti.mindbowsercontactapp.model.ContactsModal;
import com.kshiti.mindbowsercontactapp.model.ContactsModalWithPK;

import java.util.ArrayList;

public class DeletedActivity extends AppCompatActivity {
    private ArrayList<ContactsModalWithPK> contactsModalArrayList;
    private RecyclerView contactRV;
    private DeletedRVAdapter deletedRVAdapter;
    private ProgressBar loadingPB;
    DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
    String photoUriDb = "";
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deleted);
        contactsModalArrayList = new ArrayList<>();
        contactRV = findViewById(R.id.idRVDeleted);
        loadingPB = findViewById(R.id.idPBLoading);
        textView = findViewById(R.id.text);
        getSupportActionBar().setTitle("Deleted Contacts");

        getDataFromDatabase();
        prepareContactRV();
    }
    private void prepareContactRV() {
        deletedRVAdapter = new DeletedRVAdapter(this, contactsModalArrayList);
        contactRV.setLayoutManager(new LinearLayoutManager(this));
        contactRV.setAdapter(deletedRVAdapter);
    }

    public void getDataFromDatabase(){

        myRef.child("Deleted Contacts").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                int count = (int) snapshot.getChildrenCount();
                if (count==0){
                    textView.setVisibility(View.VISIBLE);
                }
                contactsModalArrayList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    String userName = (String) dataSnapshot.child("userName").getValue();
                    String contactNo = (String) dataSnapshot.child("contactNumber").getValue();
                    String pushkey = dataSnapshot.getKey();
                    boolean IsFav = (boolean) dataSnapshot.child("favourite").getValue();
                    if (dataSnapshot.hasChild("photoUri")) {
                        photoUriDb = (String) dataSnapshot.child("photoUri").getValue();
                    } else {
                        photoUriDb = "";
                    }

                    ContactsModalWithPK contactsModalWithPK = new ContactsModalWithPK(pushkey, new ContactsModal(userName, contactNo, photoUriDb, IsFav));
                    contactsModalArrayList.add(contactsModalWithPK);
                }

                deletedRVAdapter.notifyDataSetChanged();
                loadingPB.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}