package com.kshiti.mindbowsercontactapp.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kshiti.mindbowsercontactapp.R;
import com.kshiti.mindbowsercontactapp.adapters.FavouriteRVAdapter;
import com.kshiti.mindbowsercontactapp.model.ContactsModal;
import com.kshiti.mindbowsercontactapp.model.ContactsModalWithPK;

import java.util.ArrayList;

public class FavouritesActivity extends AppCompatActivity implements FavouriteRVAdapter.OnContactClickListener {
    private ArrayList<ContactsModalWithPK> contactsModalArrayList;
    private RecyclerView contactRV;
    private FavouriteRVAdapter favouriteRVAdapter;
    private ProgressBar loadingPB;
    DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
    String photoUriDb = "";
    Button callBtn, smsBtn;
    private static final int REQUEST_PHONE_CALL= 1;
    TextView textView;
    ImageView cancelImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);
        contactsModalArrayList = new ArrayList<>();
        contactRV = findViewById(R.id.idRVFavourites);
        loadingPB = findViewById(R.id.idPBLoading);
        textView = findViewById(R.id.text);
        getSupportActionBar().setTitle("Favourite Contacts");

        getDataFromDatabase();
        prepareContactRV();
    }
    private void prepareContactRV() {
        favouriteRVAdapter = new FavouriteRVAdapter(this, contactsModalArrayList, this::onContactClick);
        contactRV.setLayoutManager(new LinearLayoutManager(this));
        contactRV.setAdapter(favouriteRVAdapter);
    }

    public void getDataFromDatabase(){

        myRef.child("Contacts").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                int count = (int) snapshot.getChildrenCount();
                if (count==0){
                    textView.setVisibility(View.VISIBLE);
                }
                contactsModalArrayList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    boolean IsFav = (boolean) dataSnapshot.child("favourite").getValue();
                    if (IsFav) {
                        String userName = (String) dataSnapshot.child("userName").getValue();
                        String contactNo = (String) dataSnapshot.child("contactNumber").getValue();
                        String pushkey = dataSnapshot.getKey();
                        if (dataSnapshot.hasChild("photoUri")) {
                            photoUriDb = (String) dataSnapshot.child("photoUri").getValue();
                        } else {
                            photoUriDb = "";
                        }

                        ContactsModalWithPK contactsModalWithPK = new ContactsModalWithPK(pushkey, new ContactsModal(userName, contactNo, photoUriDb, IsFav));
                        contactsModalArrayList.add(contactsModalWithPK);
                    }
                }

                favouriteRVAdapter.notifyDataSetChanged();
                loadingPB.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    @Override
    public void onContactClick(int position) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.call_dialog);
        dialog.show();
        callBtn = dialog.findViewById(R.id.callBtn);
        smsBtn = dialog.findViewById(R.id.smsBtn);
        cancelImage = dialog.findViewById(R.id.cancelImg);

        callBtn.setOnClickListener(v -> {
            call(position);
        });

        smsBtn.setOnClickListener(v -> {
            sendSMS(position);
        });
        cancelImage.setOnClickListener(view -> dialog.dismiss());
    }

    private void call(int position)
    {

        String phoneNumber = contactsModalArrayList.get(position).getContactsModal().getContactNumber().trim();
        Log.wtf("phoneNumber",phoneNumber);

        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
        if (ActivityCompat.checkSelfPermission(FavouritesActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(FavouritesActivity.this, new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
            return;
        }
        startActivity(callIntent);
    }

    private void sendSMS(int position){
        String phoneNumber = contactsModalArrayList.get(position).getContactsModal().getContactNumber().trim();
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phoneNumber, null)));
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}