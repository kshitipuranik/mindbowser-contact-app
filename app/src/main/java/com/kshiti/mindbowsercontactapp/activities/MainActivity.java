package com.kshiti.mindbowsercontactapp.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.kshiti.mindbowsercontactapp.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button contactBtn, favouritesBtn, deletedBtn;
    RelativeLayout parentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Home");
        contactBtn = findViewById(R.id.contactBtn);
        favouritesBtn = findViewById(R.id.favouritesBtn);
        deletedBtn = findViewById(R.id.deletedBtn);
        parentLayout = findViewById(R.id.parentLayout);
        contactBtn.setOnClickListener(this);
        favouritesBtn.setOnClickListener(this);
        deletedBtn.setOnClickListener(this);

        if(!isNetworkAvailable()){
            showSnackbar();
        }

    }
    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.contactBtn:
                Intent intent = new Intent(this, ContactsActivity.class);
                startActivity(intent);
                break;
            case R.id.favouritesBtn:
                Intent intent2 = new Intent(this, FavouritesActivity.class);
                startActivity(intent2);
                break;
            case R.id.deletedBtn:
                Intent intent3 = new Intent(this, DeletedActivity.class);
                startActivity(intent3);
                break;

        }

    }
    private boolean isNetworkAvailable()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void showSnackbar(){
        Snackbar.make(parentLayout, "Please check internet connection...", Snackbar.LENGTH_LONG)
                .setAction("CLOSE", view -> {
                })
                .setDuration(10000)
                .setActionTextColor(getResources().getColor(android.R.color.holo_green_dark ))
                .show();
    }


    @Override
    public void onBackPressed() {
        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Really exit?")
                .setContentText("Are you sure, you want to exit?")
                .setConfirmText("Yes")
                .setCancelText("No")
                .showCancelButton(true)
                .setConfirmClickListener(sweetAlertDialog -> {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    intent.putExtra("EXIT", true);
                    startActivity(intent);
                    System.exit(0);
                    finish();
                })
                .setCancelClickListener(sweetAlertDialog -> sweetAlertDialog.dismissWithAnimation()).show();
    }
}