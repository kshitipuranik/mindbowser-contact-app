package com.kshiti.mindbowsercontactapp.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kshiti.mindbowsercontactapp.R;
import com.kshiti.mindbowsercontactapp.model.ContactsModalWithPK;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ContactRVAdapter extends RecyclerView.Adapter<ContactRVAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ContactsModalWithPK> contactsModalArrayList;
    DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
    OnContactClickListener onContactClickListener;

    public ContactRVAdapter(Context context, ArrayList<ContactsModalWithPK> contactsModalArrayList, OnContactClickListener onContactClickListener) {
        this.context = context;
        this.contactsModalArrayList = contactsModalArrayList;
        this.onContactClickListener = onContactClickListener;
    }

    @NonNull
    @Override
    public ContactRVAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ContactRVAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.contacts_rv_item, parent, false), onContactClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactRVAdapter.ViewHolder holder, int position) {
        ContactsModalWithPK modal = contactsModalArrayList.get(position);
        holder.contactTV.setText(modal.getContactsModal().getUserName());
        holder.contactTVNumber.setText(""+modal.getContactsModal().getContactNumber());

        if (!(modal.getContactsModal().getPhotoUri().equals("null") )){
            holder.contactIV.setImageURI(Uri.parse(modal.getContactsModal().getPhotoUri()));
        }
        else{
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getRandomColor();

            TextDrawable drawable2 = TextDrawable.builder().beginConfig()
                    .width(100)
                    .height(100)
                    .endConfig()
                    .buildRound(modal.getContactsModal().getUserName().substring(0, 1), color);
            holder.contactIV.setImageDrawable(drawable2);
        }

        myRef.child("Contacts").child(modal.getPushKey()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                boolean isFavourite = (boolean) snapshot.child("favourite").getValue();
                if (isFavourite){
                    holder.favouriteBtn.setImageResource(R.drawable.ic_filled_favourite);
                }
                else{
                    holder.favouriteBtn.setImageResource(R.drawable.ic_favourite);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        holder.favouriteBtn.setOnClickListener(view ->
                myRef.child("Contacts").child(modal.getPushKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        boolean isFavourite = (boolean) snapshot.child("favourite").getValue();
                        if (isFavourite){
                            myRef.child("Contacts").child(modal.getPushKey()).child("favourite").setValue(false);
                            holder.favouriteBtn.setImageResource(R.drawable.ic_favourite);
                        }
                        else{
                            myRef.child("Contacts").child(modal.getPushKey()).child("favourite").setValue(true);
                            holder.favouriteBtn.setImageResource(R.drawable.ic_filled_favourite);

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                }));

        holder.deleteBtn.setOnClickListener(view ->

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Really Delete?")
                        .setContentText("Are you sure you want to Delete this contact?")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .showCancelButton(true)
                        .setConfirmClickListener(sweetAlertDialog -> {
                            myRef.child("Contacts").child(modal.getPushKey()).removeValue();
                            myRef.child("Deleted Contacts").child(modal.getPushKey()).setValue(modal.getContactsModal());
                            contactsModalArrayList.remove(position);
                            sweetAlertDialog.dismissWithAnimation();
                        })
                        .setCancelClickListener(sweetAlertDialog -> sweetAlertDialog.dismissWithAnimation()).show());
    }

    @Override
    public int getItemCount() {
        return contactsModalArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView contactIV;
        private TextView contactTV;
        private TextView contactTVNumber;
        private ImageView deleteBtn, favouriteBtn;
        OnContactClickListener onContactClickListener;
        CardView cardView;

        public ViewHolder(@NonNull View itemView, OnContactClickListener onContactClickListener) {
            super(itemView);

            this.onContactClickListener = onContactClickListener;
            contactIV = itemView.findViewById(R.id.idIVContact);
            contactTV = itemView.findViewById(R.id.idTVContactName);
            contactTVNumber = itemView.findViewById(R.id.idTVContactNo);
            favouriteBtn = itemView.findViewById(R.id.favouriteIcon);
            deleteBtn = itemView.findViewById(R.id.deleteIcon);
            cardView = itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v)
        {

            onContactClickListener.onContactClick(getAdapterPosition());
        }
    }
    public interface OnContactClickListener
    {
        void onContactClick(int position);
    }

}