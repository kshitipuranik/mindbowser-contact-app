package com.kshiti.mindbowsercontactapp.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kshiti.mindbowsercontactapp.R;
import com.kshiti.mindbowsercontactapp.model.ContactsModalWithPK;

import java.util.ArrayList;

public class DeletedRVAdapter extends RecyclerView.Adapter<DeletedRVAdapter.ViewHolder> {
    private Context context;
    private ArrayList<ContactsModalWithPK> contactsModalArrayList;
    DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();

    public DeletedRVAdapter(Context context, ArrayList<ContactsModalWithPK> contactsModalArrayList) {
        this.context = context;
        this.contactsModalArrayList = contactsModalArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.deleted_rv_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ContactsModalWithPK modal = contactsModalArrayList.get(position);
        holder.contactTV.setText(modal.getContactsModal().getUserName());
        holder.contactTVNumber.setText(""+modal.getContactsModal().getContactNumber());

        if (!(modal.getContactsModal().getPhotoUri().equals("null") )){
            holder.contactIV.setImageURI(Uri.parse(modal.getContactsModal().getPhotoUri()));
        }
        else{
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getRandomColor();

            TextDrawable drawable2 = TextDrawable.builder().beginConfig()
                    .width(100)
                    .height(100)
                    .endConfig()
                    .buildRound(modal.getContactsModal().getUserName().substring(0, 1), color);
            holder.contactIV.setImageDrawable(drawable2);
        }

        holder.restoreBtn.setOnClickListener(view ->

                myRef.child("Contacts").child(modal.getPushKey()).setValue(modal.getContactsModal()).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        myRef.child("Deleted Contacts").child(modal.getPushKey()).removeValue();
                        Toast.makeText(context, "Contact restored successfully...", Toast.LENGTH_SHORT).show();
                    }
          }));
    }

    @Override
    public int getItemCount() {
        return contactsModalArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView contactIV;
        private TextView contactTV;
        private TextView contactTVNumber;
        private ImageView restoreBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            contactIV = itemView.findViewById(R.id.idIVContact);
            contactTV = itemView.findViewById(R.id.idTVContactName);
            contactTVNumber = itemView.findViewById(R.id.idTVContactNo);
            restoreBtn = itemView.findViewById(R.id.restoreIcon);
        }

    }


}
