package com.kshiti.mindbowsercontactapp.model;

public class ContactsModal {
    private String userName;
    private String contactNumber;
    private String photoUri;
    private boolean favourite;


    public ContactsModal(String userName, String contactNumber, String photoUri, boolean favourite) {
        this.userName = userName;
        this.contactNumber = contactNumber;
        this.photoUri = photoUri;
        this.favourite = favourite;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }
}
