package com.kshiti.mindbowsercontactapp.model;

public class ContactsModalWithPK {
    private String pushKey;
    ContactsModal contactsModal;

    public ContactsModalWithPK(String pushKey, ContactsModal contactsModal) {
        this.pushKey = pushKey;
        this.contactsModal = contactsModal;
    }

    public String getPushKey() {
        return pushKey;
    }

    public ContactsModal getContactsModal() {
        return contactsModal;
    }
}

